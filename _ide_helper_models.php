<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\BaseModel
 *
 * @method static \Illuminate\Database\Eloquent\Builder|BaseModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BaseModel newQuery()
 * @method static \Illuminate\Database\Query\Builder|BaseModel onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|BaseModel query()
 * @method static \Illuminate\Database\Query\Builder|BaseModel withTrashed()
 * @method static \Illuminate\Database\Query\Builder|BaseModel withoutTrashed()
 */
	class BaseModel extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Evaluation
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Evaluation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Evaluation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Evaluation query()
 */
	class Evaluation extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ExpenseReport
 *
 * @method static \Illuminate\Database\Eloquent\Builder|ExpenseReport newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ExpenseReport newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ExpenseReport query()
 */
	class ExpenseReport extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Formation
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Formation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Formation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Formation query()
 */
	class Formation extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\HolidayRequest
 *
 * @method static \Illuminate\Database\Eloquent\Builder|HolidayRequest newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HolidayRequest newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HolidayRequest query()
 */
	class HolidayRequest extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Job
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Job newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Job newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Job query()
 */
	class Job extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Payslip
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Payslip newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Payslip newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Payslip query()
 */
	class Payslip extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Service
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Service newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Service newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Service query()
 * @method static \Illuminate\Database\Eloquent\Builder|Service responsable()
 */
	class Service extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Task
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Task newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Task newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Task query()
 */
	class Task extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 */
	class User extends \Eloquent {}
}

