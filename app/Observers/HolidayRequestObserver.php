<?php

namespace App\Observers;

use App\Models\HolidayRequest;
use Mail;

class HolidayRequestObserver
{
    /**
     * Handle the HolidayRequest "created" event.
     *
     * @param  \App\Models\HolidayRequest  $holidayRequest
     * @return void
     */
    public function created(HolidayRequest $holidayRequest)
    {
        //
    }

    /**
     * Handle the HolidayRequest "updated" event.
     *
     * @param  \App\Models\HolidayRequest  $holidayRequest
     * @return void
     */
    public function updated(HolidayRequest $holidayRequest)
    {
        if ($holidayRequest->isDirty('status')){
            $user = $holidayRequest->user()->first();
            $to_name = $user->firstname." ".$user->lastname;
            $to_email = $user->email;
            $data = array(
                'name' => $to_name,
                "body" => "Le statut de votre demande de congé du ". $holidayRequest->start_date. " au ".$holidayRequest->end_date ." a été passée à ".$holidayRequest->status
            );
            Mail::send('mail', $data, function ($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)->subject('Notification demande de congés');
                $message->from('zaturgo@gmail.com', 'RH-App');
            });
        }
    }

    /**
     * Handle the HolidayRequest "deleted" event.
     *
     * @param  \App\Models\HolidayRequest  $holidayRequest
     * @return void
     */
    public function deleted(HolidayRequest $holidayRequest)
    {
        //
    }

    /**
     * Handle the HolidayRequest "restored" event.
     *
     * @param  \App\Models\HolidayRequest  $holidayRequest
     * @return void
     */
    public function restored(HolidayRequest $holidayRequest)
    {
        //
    }

    /**
     * Handle the HolidayRequest "force deleted" event.
     *
     * @param  \App\Models\HolidayRequest  $holidayRequest
     * @return void
     */
    public function forceDeleted(HolidayRequest $holidayRequest)
    {
        //
    }
}
