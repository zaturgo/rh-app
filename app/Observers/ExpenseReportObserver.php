<?php

namespace App\Observers;

use App\Models\ExpenseReport;
use Mail;

class ExpenseReportObserver
{
    /**
     * Handle the ExpenseReport "created" event.
     *
     * @param  \App\Models\ExpenseReport  $expenseReport
     * @return void
     */
    public function created(ExpenseReport $expenseReport)
    {

    }

    /**
     * Handle the ExpenseReport "updated" event.
     *
     * @param  \App\Models\ExpenseReport  $expenseReport
     * @return void
     */
    public function updated(ExpenseReport $expenseReport)
    {
        if ($expenseReport->isDirty('status')){
            $user = $expenseReport->user()->first();
            $to_name = $user->firstname." ".$user->lastname;
            $to_email = $user->email;
            $data = array(
                'name' => $to_name,
                "body" => "Le statut de votre note de frais ". $expenseReport->comment." a été passée à ".$expenseReport->status
            );
            Mail::send('mail', $data, function ($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)->subject('Notification note de frais');
                $message->from('zaturgo@gmail.com', 'RH-App');
            });
        }
    }

    /**
     * Handle the ExpenseReport "deleted" event.
     *
     * @param  \App\Models\ExpenseReport  $expenseReport
     * @return void
     */
    public function deleted(ExpenseReport $expenseReport)
    {
        //
    }

    /**
     * Handle the ExpenseReport "restored" event.
     *
     * @param  \App\Models\ExpenseReport  $expenseReport
     * @return void
     */
    public function restored(ExpenseReport $expenseReport)
    {
        //
    }

    /**
     * Handle the ExpenseReport "force deleted" event.
     *
     * @param  \App\Models\ExpenseReport  $expenseReport
     * @return void
     */
    public function forceDeleted(ExpenseReport $expenseReport)
    {
        //
    }
}
