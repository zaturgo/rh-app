<?php

namespace App\Providers;

use App\Models\ExpenseReport;
use App\Models\HolidayRequest;
use App\Models\User;
use App\Observers\ExpenseReportObserver;
use App\Observers\HolidayRequestObserver;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        HolidayRequest::observe(HolidayRequestObserver::class);
        ExpenseReport::observe(ExpenseReportObserver::class);
    }
}
