<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class ExpenseReport extends BaseModel
{
    protected $guarded = [];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
