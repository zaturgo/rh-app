<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Evaluation extends BaseModel
{
    protected $with=['user'];
    public function responsible(){
        return $this->belongsTo('App\Models\User','resp_id');
    }
    public function user(){
        return $this->belongsTo('App\Models\User','user_id');
    }
}
