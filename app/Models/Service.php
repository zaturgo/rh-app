<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends BaseModel
{
    //protected $withCount = ['users'];
    //protected $with = ['users'];
    public function users(){
        return $this->hasMany('User');
    }
    public function formations(){
        return $this->belongsToMany('Formation');
    }
    public function scopeResponsable($query){
        return $query->users()->where('role','responsable')->first();
    }
}
