<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Formation extends BaseModel
{
    public function responsable(){
        return $this->belongsTo('User');
    }
    public function users(){
        return $this->belongsToMany('User');
    }
    public function services(){
        return $this->belongsToMany('Service');
    }
}
