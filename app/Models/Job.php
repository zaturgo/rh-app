<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Job extends BaseModel
{
    protected $with=['user'];
    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
