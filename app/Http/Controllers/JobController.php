<?php

namespace App\Http\Controllers;

use App\Models\Job;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Mail;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        try{
            return new JsonResponse([
                'message'=>Job::all()
            ],200);
        }catch(QueryException $e){
            return new JsonResponse([
                'message'=>$e
            ],500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        try{
            return new JsonResponse([
                'message'=>Job::create($request->all())
            ],200);
        }catch(QueryException $e){
            return new JsonResponse([
                'message'=>$e
            ],500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function show(Job $job)
    {
        return $job;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function edit(Job $job)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Job $job)
    {
        $job->update($request->all());

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function destroy(Job $job)
    {
        $job->delete();
    }
    public function applyToJob(Job $job, Request $request){
        $user = $job->user()->first();
        $to_name = $user->firstname." ".$user->lastname;
        $to_email = $user->email;
        $data = array(
            'name' => $to_name,
            "body" => "Voici une candidature pour le poste  ". $job->name. "de la part de ".$request->name." - ".$request->email
        );
        $pathCV = $request->file('cv')->store('candidates');
        $pathLetter = $request->file('letter')->store('candidates');
        Mail::send('mail', $data, function ($message) use ($to_name, $to_email,$pathCV,$pathLetter) {
            $message->to($to_email, $to_name)->subject('Notification candidature');
            $message->from('zaturgo@gmail.com', 'RH-App');
            $message->attach($pathCV);
            $message->attach($pathLetter);
        });
            return new JsonResponse([
                'message'=>'Candidature bien reçue'
            ],200);

    }
}
