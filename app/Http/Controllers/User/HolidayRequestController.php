<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\HolidayRequest;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class HolidayRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(User $user)
    {
        try{
            return new JsonResponse([
                'message'=>$user->holidayRequests()->get()
            ],200);
        }catch(QueryException $e){
            return new JsonResponse([
                'message'=>$e
            ],500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HolidayRequest  $holidayRequest
     * @return \Illuminate\Http\Response
     */
    public function show(HolidayRequest $holidayRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HolidayRequest  $holidayRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(HolidayRequest $holidayRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HolidayRequest  $holidayRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HolidayRequest $holidayRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HolidayRequest  $holidayRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(HolidayRequest $holidayRequest)
    {
    }
    public function remainingDays(User $user){
        $months = Carbon::parse($user->created_at)->diffInMonths(Carbon::now());
        $daysRequested=0;
        foreach($user->holidayRequests()->where('type','Paid')->where('status','Accepted')->get() as $holidayRequest){
            $daysRequested+= Carbon::parse($holidayRequest->start_date)->diffInWeekDays(Carbon::parse($holidayRequest->end_date));
        }
        return ($months/10*25)-$daysRequested;
    }
}
