<?php

namespace App\Http\Controllers;

use App\Models\ExpenseReport;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Mail;

class ExpenseReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        try {
            return new JsonResponse([
                'message' => ExpenseReport::where('status', 'Waiting')->with('user')->get()
            ], 200);
        } catch (QueryException $e) {
            return new JsonResponse([
                'message' => $e
            ], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        try {
            return new JsonResponse([
                'message' => ExpenseReport::create($request->all())
            ], 200);
        } catch (QueryException $e) {
            return new JsonResponse([
                'message' => $e
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\ExpenseReport $expenseReport
     * @return \Illuminate\Http\Response
     */
    public function show(ExpenseReport $expenseReport)
    {
        return $expenseReport;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\ExpenseReport $expenseReport
     * @return \Illuminate\Http\Response
     */
    public function edit(ExpenseReport $expenseReport)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ExpenseReport $expenseReport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExpenseReport $expenseReport)
    {
        $expenseReport->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\ExpenseReport $expenseReport
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExpenseReport $expenseReport)
    {
        $expenseReport->delete();
    }
}
