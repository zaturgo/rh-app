<?php

namespace App\Http\Controllers;

use App\Models\HolidayRequest;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class HolidayRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        try{
            return new JsonResponse([
                'message'=>HolidayRequest::where('status','Waiting')->with('user')->get()
            ],200);
        }catch(QueryException $e){
            return new JsonResponse([
                'message'=>$e
            ],500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $startDate = Carbon::parse($request->start_date);
        $endDate = Carbon::parse($request->end_date);
        if ($request->startDayPeriod=='Morning')$startDate->addHours(8);
            else $startDate->addHours(12);
        if ($request->endDayPeriod=='Morning')$endDate->addHours(12);
            else $endDate->addHours(17);
        try{
            HolidayRequest::create(array_merge($request->except(['start_date','end_date','endDayPeriod','startDayPeriod']),['start_date'=>$startDate,'end_date'=>$endDate]));
            return new JsonResponse([
                'message'=>'success'
            ],200);
        }catch(QueryException $e){
            return new JsonResponse([
                'message'=>$e
            ],500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HolidayRequest  $holidayRequest
     * @return \Illuminate\Http\Response
     */
    public function show(HolidayRequest $holidayRequest)
    {
        return $holidayRequest;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HolidayRequest  $holidayRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(HolidayRequest $holidayRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HolidayRequest  $holidayRequest
     * @return JsonResponse
     */
    public function update(Request $request, HolidayRequest $holidayRequest)
    {
        $startDate = Carbon::parse($request->start_date);
        $endDate = Carbon::parse($request->end_date);
        if ($request->startDayPeriod=='Morning')$startDate->addHours(8);
        else $startDate->addHours(12);
        if ($request->endDayPeriod=='Morning')$endDate->addHours(12);
        else $endDate->addHours(17);
        try{
            $holidayRequest->update(array_merge($request->except(['start_date','end_date','endDayPeriod','startDayPeriod']),['start_date'=>$startDate,'end_date'=>$endDate]));
            return new JsonResponse([
                'message'=>'success'
            ],200);
        }catch(QueryException $e){
            return new JsonResponse([
                'message'=>$e
            ],500);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HolidayRequest  $holidayRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(HolidayRequest $holidayRequest)
    {
        $holidayRequest->delete();
    }
}
