<?php

namespace App\Exports;

use App\Models\ExpenseReport;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;

class ExpenseReportExport implements FromCollection
{
    protected $user;
    /**
     * ExpenseReportExport constructor.
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }


    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return $this->user->expenseReports()->get();
    }
}
