<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users',function(Blueprint $table){
            $table->foreign('service_id')
                ->references('id')
                ->on('services');
        });
        Schema::table('jobs',function(Blueprint $table){
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
        Schema::table('tasks',function(Blueprint $table){
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
        Schema::table('payslips',function(Blueprint $table){
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
        Schema::table('holiday_requests',function(Blueprint $table){
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
        Schema::table('formations',function(Blueprint $table){
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
        Schema::table('expense_reports',function(Blueprint $table){
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
        Schema::table('formation_user',function(Blueprint $table){
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
            $table->foreign('formation_id')
                ->references('id')
                ->on('formations');
        });
        Schema::table('formation_service',function(Blueprint $table){
            $table->foreign('service_id')
                ->references('id')
                ->on('services');
            $table->foreign('formation_id')
                ->references('id')
                ->on('formations');
        });
        Schema::table('evaluations',function(Blueprint $table){
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
            $table->foreign('resp_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
