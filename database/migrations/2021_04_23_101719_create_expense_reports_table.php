<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpenseReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type',['Transport','Food','Lodging','Other']);
            $table->text('comment')->nullable();
            $table->float('amount');
            $table->unsignedInteger('user_id');
            $table->timestamp('date');
            $table->enum('currency',['EUR','USD','CHF','GBP','Other']);
            $table->enum('payment_type',['CB','Cash','Transfer']);
            $table->enum('status',['Waiting','Validated','Refused']);
            $table->string('url');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense_reports');
    }
}
