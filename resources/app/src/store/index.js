import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
      connectedUser:null,
  },
  mutations: {
      setUser (state,user) {
          state.connectedUser=user
      }
  },
  actions: {
      fetchUser(context,sub){
          axios.get('/api/userAuth0/'+sub).then(res=>{
              context.commit('setUser',res.data)
          })
      }
  },
  modules: {
  }
})
