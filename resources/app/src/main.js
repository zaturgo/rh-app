import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from "axios";
import VueAxios from 'vue-axios'
// import the styles
import 'vue-good-table/dist/vue-good-table.css'
// Import the Auth0 configuration
import {domain, clientId} from "../auth_config.json";

// Import the plugin here
import {Auth0Plugin} from "./auth";
// Install the authentication plugin here
Vue.use(Auth0Plugin, {
    domain,
    clientId,
    onRedirectCallback: appState => {
        router.push(
            appState && appState.targetUrl
                ? appState.targetUrl
                : window.location.pathname
        );
    }
});


//Message toast
import Toasted from 'vue-toasted';

//FontAwesome
import '@fortawesome/fontawesome-free/js/fontawesome';
import '@fortawesome/fontawesome-free/js/solid';
import '@fortawesome/fontawesome-free/js/regular';
import '@fortawesome/fontawesome-free/js/brands';

//AntD
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
Vue.config.productionTip = false;

Vue.use(Antd);

import { Button } from 'ant-design-vue';
Vue.use(Button);
import { FormModel } from 'ant-design-vue';
Vue.use(FormModel);


import moment from "moment";
import VueGoodTablePlugin from "vue-good-table";
Vue.use(VueAxios, axios)
Vue.config.productionTip = false
Vue.use(VueGoodTablePlugin);
/**
 * Fonction pour l'affichage d'une date : DD/MM/YYYY
 */
Vue.filter('formatDate', function (value) {
    if (value) {
        return moment(String(value)).format('DD/MM/YYYY')
    }
})
Vue.mixin({
    methods: {
        getToken: async function () {
          return await this.$auth.getTokenSilently();
        },
        getUser: function () {
          return this.$store.state.connectedUser;
        },
    },
})


/**
 * Fonction pour l'affichage d'une date et heure : DD/MM/YYYY HH:mm
 */
Vue.filter('formatDateHour', function (value) {
    if (value) {
        return moment(String(value)).format('DD/MM/YYYY HH:mm')
    }
})
import { getInstance } from "./auth/index";
const instance = getInstance()
instance.$watch("isAuthenticated", async isAuthenticated => {
    if (isAuthenticated === true) {
            axios.defaults.headers.common = {'Authorization': `Bearer ${await instance.getTokenSilently()}`}
            store.dispatch('fetchUser', instance.user.sub);

    }
});
/**
 * Message toast, aligné en haut à droite pour 2500 millisecondes
 */
Vue.use(Toasted, {
    router,
    duration: 2500,
    position: 'top-right'
});



new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
