import Vue from 'vue'
import VueRouter from 'vue-router'
import ManageHolidays from "../views/Holidays/ManageHolidays";
import CreateHRequest from "../views/Holidays/CreateHRequest";
import ManageReports from "../views/ExpenseReports/ManageReports";
import CreateExpenseReport from "../views/ExpenseReports/CreateExpenseReport";
import ManageTask from "../views/Tasks/ManageTask";
import CreateTask from "../views/Tasks/CreateTask";
import HomePage from "../views/HomePage";
import ManageRecruitments from "../views/Recruitments/ManageRecruitments";
import CreateJobOffer from "../views/Recruitments/CreateJobOffer";
import {authGuard} from "../auth/authGuard";
import ManageFormations from "../views/Formation/ManageFormation";
import CreateFormation from "../views/Formation/CreateFormation";
import ManageEvaluations from "../views/Evaluations/ManageEvaluations"
import ManageRoles from "../views/RH/ManageRoles"
import Services from "../views/RH/Services";
import FormationInscription from "../views/Formation/FormationInscription";

Vue.use(VueRouter)


const routes = [
    {
        path: '/',
        name: 'HomePage',
        component: HomePage
    },
    {
        path: '/holidays',
        name: 'ManageHolidays',
        component: ManageHolidays,
        beforeEnter: authGuard
    },
    {
        path: '/holidays/createHRequest/:id?',
        name: 'CreateHRequest',
        component: CreateHRequest,
        beforeEnter: authGuard
    },
    {
        path: '/expenses',
        name: 'ManageReports',
        component: ManageReports,
        beforeEnter: authGuard
    },
    {
        path:'/expenses/CreateExpenseReport/:id?',
        name: 'CreateExpenseReport',
        component: CreateExpenseReport,
        beforeEnter: authGuard
    },
    {
        path: '/tasks',
        name: 'ManageTask',
        component: ManageTask,
        beforeEnter: authGuard
    },
    {
        path: '/tasks/CreateTask/:id?',
        name: 'CreateTask',
        component: CreateTask
    },
    {
        path: '/recruitments',
        name: 'ManageRecruitments',
        component: ManageRecruitments,
        beforeEnter: authGuard
    },
    {
        path: '/recruitments/CreateJobOffer/:id?',
        name: 'CreateJobOffer',
        component: CreateJobOffer,
        beforeEnter: authGuard
    },
    {
        path: '/formations',
        name: 'ManageFormations',
        component: ManageFormations,
        beforeEnter: authGuard
    },
    {
        path: '/formations/CreateFormation/:id?',
        name: 'CreateFormation',
        component: CreateFormation,
        beforeEnter: authGuard
    },  {
        path: '/formations/inscription',
        name: 'FormationInscription',
        component: FormationInscription,
        beforeEnter: authGuard
    }, {
        path: '/services',
        name: 'Services',
        component: Services,
        beforeEnter: authGuard
    },
    {
        path: '/evaluations',
        name: 'ManageEvaluations',
        component: ManageEvaluations,
        beforeEnter: authGuard
    },
    {
        path: '/roles',
        name: 'ManageRoles',
        component: ManageRoles,
        beforeEnter: authGuard
    }
]


const router = new VueRouter({
    mode:'history',
  routes
})


export default router
