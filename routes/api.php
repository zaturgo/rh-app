<?php

use App\Http\Middleware\CheckJWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => 'jwt'], function() {
    Route::resource('user', 'App\Http\Controllers\UserController',['except'=>'store']);
    Route::get('userAuth0/{sub}', 'App\Http\Controllers\UserController@userAuth0');
    Route::resource('user.holidayRequest', 'App\Http\Controllers\User\HolidayRequestController');
    Route::resource('user.formations', 'App\Http\Controllers\User\FormationController');
    Route::resource('formations', 'App\Http\Controllers\FormationController');
    Route::get('user/{user}/exportExpenses', 'App\Http\Controllers\User\ExpenseReportController@export');
    Route::resource('user.expenseReport', 'App\Http\Controllers\User\ExpenseReportController');
    Route::get('user/{user}/getRemainingDays', 'App\Http\Controllers\User\HolidayRequestController@remainingDays');
    Route::resource('task', 'App\Http\Controllers\TaskController');
    Route::post('tasks/getTasksOfDay', 'App\Http\Controllers\TaskController@getTasksOfDay');
    Route::resource('service', 'App\Http\Controllers\ServiceController');
    Route::resource('holidayRequest', 'App\Http\Controllers\HolidayRequestController');
    Route::resource('expenseReport', 'App\Http\Controllers\ExpenseReportController');
    Route::resource('formation', 'App\Http\Controllers\FormationController');
    Route::resource('payslip', 'App\Http\Controllers\PayslipController');
    Route::resource('evaluation', 'App\Http\Controllers\EvaluationController');
});
Route::group(['middleware' => 'custom'], function() {
    Route::resource('job', 'App\Http\Controllers\JobController',['except'=>'index']);
    Route::post('user', 'App\Http\Controllers\UserController@store');
    Route::post('job/{job}/apply', 'App\Http\Controllers\JobController@applyToJob');
    Route::get('job', 'App\Http\Controllers\JobController@index');
});
